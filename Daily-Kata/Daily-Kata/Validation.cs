﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daily_Kata
{
    public class Validation
    {

        public static bool ValidName(string name)
        {
            string[] splitted = name.Split(" ");
            bool[] checks = new bool[splitted.Length];
            bool status = true;

            // Edge case if name does not contain 2 terms or more
            if (splitted.Length <= 1) return false;
            
            for(int i = 0; i < splitted.Length; i++)
            {


                if(splitted[i] == splitted[^1])
                {
                    checks[i] = !ContainsDotAtEnd(splitted[i]) && !LongerThan2(splitted[i]);
                }

                if(ContainsDotAtEnd(splitted[i])) 
                {
                    checks[i] = InitialIsUpperCase(splitted[i]) && LongerThan2(splitted[i]);
                }
                else
                {
                    checks[i] = !ContainsDotAtEnd(splitted[i]) && InitialIsUpperCase(splitted[i]) ;
                }
            }

            foreach(bool check in checks)
            {
                if(check == false)
                {
                    status = false;
                    break;
                }
            }

            return status;
        }

        private static bool LongerThan2(string initial)
        {
            return initial.Length > 2;
        }

        private static bool ContainsDotAtEnd(string word)
        {
            return word[^1] == '.';
        }

        private static bool InitialIsUpperCase(string initial)
        {
            return Char.IsUpper(initial[0]);
        }
    }
}
