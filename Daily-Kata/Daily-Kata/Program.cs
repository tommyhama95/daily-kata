﻿using System;

namespace Daily_Kata
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 0;
            while (number >= 0)
            {
                Console.WriteLine("Hello! What number do you want romanized?");
                Int32.TryParse(Console.ReadLine(), out int n);
                number = n;
                Console.WriteLine($"Your number {number} in Roman is {Roman.ConvertToRoman(number)}");
                Console.WriteLine("~~~~~~~~~~~~~~~~~~~~");
            }


            /*
            Console.WriteLine("Hello World!");

            string word = "Hello World";
            int length = word.Length;
            string empty = "";
            string rev = RecursiveReverseString(length - 1, empty, word);
            Console.WriteLine("Printing out word");
            Console.WriteLine(rev);
            */
        }

        public static string RecursiveReverseString(int i, string newWord, string word)
        {
            if (newWord.Length != word.Length)
            {
                newWord += word[i];
                Console.WriteLine(newWord);
                RecursiveReverseString(--i, newWord, word);
            }
            return newWord;
        }
    }
}
