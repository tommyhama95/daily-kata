﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daily_Kata
{
    public class Reducto_Multiplicitum
    {
        public static int SumDigProd(params int[] numbers)
        {
            int addedSum = 0;
            foreach(int number in numbers)
            {
                addedSum += number;
            }

            int sum = addedSum;
            string sumInString = addedSum.ToString();
            while(sumInString.Length > 1)
            {
                sum = 1;
                for(int i = 0; i < sumInString.Length; i++)
                {
                    int n = Int32.Parse(sumInString[i].ToString());
                    sum *= n;
                }
                sumInString = sum.ToString();
            }
            return sum;
        }
    }
}
