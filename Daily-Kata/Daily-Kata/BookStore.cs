﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daily_Kata
{
    public class BookStore
    {

        public static double Buy(int[] books)
        {
            return CheckForDiscount(books);
        }

        private static double CheckForDiscount(int[] books)
        {
            int[] titles = books.Distinct().ToArray();

            double discount = CalculateDiscount(titles);
            return ((8 * books.Length) * discount);
        }

        private static double CalculateDiscount(int[] titles)
        {
            if (titles.Length == 2)
                return 0.95;
            if (titles.Length == 3)
                return 0.9;
            if (titles.Length == 4)
                return 0.8;

            else return 1;
        }
    }
}
