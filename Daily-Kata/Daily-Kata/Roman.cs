﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daily_Kata
{
    public class Roman
    {
        public static string ConvertToRoman(int number)
        {
            if (number >= 9000)
                return "IT'S OVER 9000!!!";
            if(number >= 4000 || number <= 0) 
                return "Does not exist"; 
            
            string roman = "";
            while(number != 0)
            {
                // Over 1000
                if(number >= 1000) { roman += "M"; number -= 1000; }

                // Range of 900 til 999
                if( number >= 900 && number < 1000) { roman += "CM"; number -= 900; }

                // Range of 500 til 899
                if(number >= 500 && number < 900) { roman += "D"; number -= 500; }

                // Range of 400 til 499
                if(number >= 400 && number < 500) { roman += "CD"; number -= 400; }

                // Add on 100 if possible
                if(number >= 100 && number < 1000) { roman += "C"; number -= 100; }

                // Blocks all numbers below hundred to not look above 100 without && checks on all statements
                if(number < 100)
                {
                    // Case of 90 and under 100
                    if(number >= 90) { roman += "XC"; number -= 90; }

                    if(number >= 50 && number < 90) { roman += "L"; number -= 50; }

                    // Edge case of range 40
                    if(number >= 40 && number < 50) { roman += "XL"; number -= 40; }    

                    // Edge case of 14
                    if(number == 14) { roman += "XIV"; number -= 14; }

                    if(number >= 10) { roman += "X"; number -= 10; }

                    // Edge case of 9
                    if(number == 9) { roman += "IX"; number -= 9; }

                    if(number >= 5 && number <= 8) { roman += "V"; number -= 5; }

                    // Edge case of 4
                    if(number == 4) { roman += "IV"; number -= 4; }

                    if(number <= 3 && number > 0) { roman += "I"; number--; }
                }
            }

            return roman;
        }
    }
}
