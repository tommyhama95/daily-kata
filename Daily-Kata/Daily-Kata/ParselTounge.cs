﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daily_Kata
{
    public class ParselTounge
    {
        public static bool CheckForParsels(string ssssentence)
        {
            int count = 0;
            string[] splitted = ssssentence.ToLower().Split(" ");

            // Original approach, nested loop
            foreach(string word in splitted)
            {
                for(int i = 0; i < word.Length; i++)
                {
                    if(word[i] == 's')
                    {
                        if(word[i] == word[i+1])
                        {
                            count++;
                            break;
                        }
                        else
                        {
                            count = -1;
                            break;
                        }
                    } 
                }
                if (count == -1)
                    return false;
            }
            return true;
        }
    }
}
