﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daily_Kata
{
    public class Casing
    {

        public static string ToCamelCase(string sentence)
        {
            string[] splitted = sentence.Split("_");
            string newSentence = splitted[0];
            for(int i = 1; i < splitted.Length; i++)
            {
                splitted[i] = Char.ToUpper(splitted[i][0]) + splitted[i].Substring(1);
                newSentence += splitted[i];
            }
            return newSentence;
        }

        public static string ToSnakeCase(string sentence)
        {
            return String.Join("", sentence.Select(c => Char.IsUpper(c) ? $"_{Char.ToLower(c)}" : c.ToString()));
        }
    }
}
