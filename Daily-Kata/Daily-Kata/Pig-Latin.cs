﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daily_Kata
{
    public class Pig_Latin
    {
        public static char[] vowels = { 'a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U' };

        public static string ConvertWithPigLatin(string word)
        {
            string convertedWord = "";
            bool containsVowel = CheckIfCharacterIsVowel(word[0]);
            string punctuation = ContainsPunctuatuon(word);

            if (punctuation.Length > 0)
                word = word.Remove(word.IndexOf(punctuation), 1);

                 
            if(containsVowel)
            {
                convertedWord = ConvertWordStartingWithVowel(word);
            }
            else
            {
                convertedWord = ConvertWordStartingWithConsonant(word);
            }
            return convertedWord + punctuation;
        }

        private static string ContainsPunctuatuon(string word)
        {
            foreach(char character in word)
            {
                if(char.IsPunctuation(character))
                {
                    return character.ToString();
                }
            }
            return "";
        }

        private static string ConvertWordStartingWithConsonant(string word)
        {
            string converted = word;
            for (int i = 0; i < word.Length; i++)
            {
                if (!CheckIfCharacterIsVowel(word[i]))
                {
                    // Make a new word from the index in the loop and append the letter at the index to the end
                    converted = converted.Remove(0, 1);
                    converted += word[i];
                } 
                else
                {
                    break;
                }
            }
            return converted + "ay";
        }

        public static string ConvertSentence(string sentence)
        {
            string[] splitted = sentence.Split(' ');
            List<string> newSentence = new();

            foreach(string word in splitted)
            {
                newSentence.Add(ConvertWithPigLatin(word));
            }
            string [] arr = newSentence.ToArray<string>();

            return String.Join(" ", arr);
        }


        private static string ConvertWordStartingWithVowel(string word)
        {
            return word + "yay";            
        }

        private static bool CheckIfCharacterIsVowel(char letter)
        {
            bool startsWithVowel = false;

            foreach(char vowel in vowels)
            {
                if (vowel == letter)
                {
                    startsWithVowel = true;
                    break;
                }
            }
            return startsWithVowel;
        }
    }
}
