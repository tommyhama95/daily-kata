﻿using Daily_Kata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Daily_Kata_Test
{
    public class Casing_Test
    {

        [Theory]
        [InlineData("helloEdabit", "hello_edabit")]
        [InlineData("isModalOpen", "is_modal_open")]
        public void CamelCaseTheSentence_ShouldConvert(string expected, string sentence)
        {
            string actual = Casing.ToCamelCase(sentence);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("hello_edabit", "helloEdabit")]
        [InlineData("get_color", "getColor")]
        public void SnakeCaseTheSentence_ShouldConvert(string expected, string sentence)
        {
            string actual = Casing.ToSnakeCase(sentence);

            Assert.Equal(expected, actual);

        }

    }
}
