﻿using Daily_Kata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Daily_Kata_Test
{
    public class ParselTounge_Test
    {

        [Fact(DisplayName = "Double consecutive occurrences of S, return true")]
        public void ParselTounge_DoubleConsecutive_ShouldReturn_True()
        {
            string ssssentence = "Sshe ssselects to eat that apple.";
            bool expected = true;
            bool actual = ParselTounge.CheckForParsels(ssssentence);

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "One Double but one single s in sentence, return false")]
        public void ParselTounge_OneSingleAndOneDoubleS_ShouldReturn_False()
        {
            string sentence = "She ssselects to eat that apple.";
            bool expected = false;
            bool actual = ParselTounge.CheckForParsels(sentence);

            Assert.Equal(expected, actual);
        }
        
        [Fact(DisplayName = "Three different words with sss, return true")]
        public void ParselTounge_ThreeOccurrences_ShouldReturn_True()
        {
            string sentence = "You ssseldom sssspeak sso boldly, sso messmerizingly.";
            bool expected = true;
            bool actual = ParselTounge.CheckForParsels(sentence);

            Assert.Equal(expected, actual);
        }
        
        [Fact(DisplayName = "Steve likes to eat pancakes, return false")]
        public void ParselTounge_SingleS_ShouldReturn_False()
        {
            string sentence = "Steve likes to eat pancakes";
            bool expected = false;
            bool actual = ParselTounge.CheckForParsels(sentence);

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "Sssteve likess to eat pancakess, return true")]
        public void ParselTounge_PancakeSteve_ShouldReturn_True()
        {
            string sentence = "Sssteve likess to eat pancakess";
            bool expected = true;
            bool actual = ParselTounge.CheckForParsels(sentence);

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "Beatrice samples lemonade, return false")]
        public void ParselTounge_BeatricesLemonade_ShouldReturn_False()
        {
            string sentence = "Beatrice samples lemonade";
            bool expected = false;
            bool actual = ParselTounge.CheckForParsels(sentence);

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "Beatrice enjoysss lemonade, return true")]
        public void ParselTounge_BeatriceEnjoysss_ShouldReturn_True()
        {
            string sentence = "Beatrice enjoysss lemonade";
            bool expected = true;
            bool actual = ParselTounge.CheckForParsels(sentence);

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "The word dont have any of the you-know-what-letter, return true")]
        public void ParselTounge_NoSSSSS_ShouldReturn_True()
        {
            string entence = "The word dont have any of the you-know-what-letter";
            bool expected = true;
            bool actual = ParselTounge.CheckForParsels(entence);

            Assert.Equal(expected, actual);
        }
    }
}
