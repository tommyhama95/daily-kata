﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Daily_Kata;

namespace Daily_Kata_Test
{
    public class Pig_Latin_Test
    {   
        [Theory]
        [InlineData("ate", "ateyay")]
        [InlineData("apple", "appleyay")]
        [InlineData("oaken", "oakenyay")]
        [InlineData("eagle", "eagleyay")]
        public void ConvertVowel_Dataset_ShouldAddYAYToEnd(string word, string expected) 
        {
            string actual = Pig_Latin.ConvertWithPigLatin(word);

            Assert.Equal(expected, actual);
        }


        [Theory]
        [InlineData("have", "avehay")]
        [InlineData("cram", "amcray")]
        [InlineData("take", "aketay")]
        [InlineData("cat", "atcay")]
        [InlineData("shrimp", "impshray")]
        [InlineData("trebuchet", "ebuchettray")]
        public void ConvertConsonant_Dataset_ShouldConvert(string word, string expected)
        {
            string actual = Pig_Latin.ConvertWithPigLatin(word);

            Assert.Equal(expected, actual);
        }


        [Theory]
        [InlineData("I like to eat honey waffles.", "Iyay ikelay otay eatyay oneyhay afflesway.")]
        public void ConvertSentence_ShouldConvert(string sentence, string expected)
        {
            string actual = Pig_Latin.ConvertSentence(sentence);

            Assert.Equal(expected, actual);
        }

    }
}