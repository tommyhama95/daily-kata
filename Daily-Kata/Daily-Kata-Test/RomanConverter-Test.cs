﻿using Daily_Kata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Daily_Kata_Test
{
    public class RomanConverter_Test
    {
        [Fact(DisplayName = "1 should be I")]
        public void RomanConverter_I()
        {
            string expected = "I";
            string actual = Roman.ConvertToRoman(1);

            Assert.Equal(expected, actual);
        }

        [Fact(DisplayName = "4 should be IV")]
        public void RomanConverter_IV()
        {
            string expected = "IV";
            string actual = Roman.ConvertToRoman(4);

            Assert.Equal(expected, actual);
        }
        
        [Fact(DisplayName = "5 should be V")]
        public void RomanConverter_V()
        {
            string expected = "V";
            string actual = Roman.ConvertToRoman(5);

            Assert.Equal(expected, actual);
        }
        
        [Fact(DisplayName = "8 should be VIII")]
        public void RomanConverter_VIII()
        {
            string expected = "VIII";
            string actual = Roman.ConvertToRoman(8);

            Assert.Equal(expected, actual);
        }
        
        [Fact(DisplayName = "9 should be IX")]
        public void RomanConverter_IX()
        {
            string expected = "IX";
            string actual = Roman.ConvertToRoman(9);

            Assert.Equal(expected, actual);
        }


        [Theory(DisplayName = "Edge cases")]
        [InlineData(14, "XIV")]
        [InlineData(19, "XIX")]
        [InlineData(22, "XXII")]
        [InlineData(40, "XL")]
        [InlineData(50, "L")]
        [InlineData(70, "LXX")]
        [InlineData(90, "XC")]
        [InlineData(100, "C")]
        [InlineData(200, "CC")]
        [InlineData(400, "CD")]
        [InlineData(444, "CDXLIV")]
        [InlineData(500, "D")]
        [InlineData(538, "DXXXVIII")]
        [InlineData(800, "DCCC")]
        [InlineData(900, "CM")]
        [InlineData(999, "CMXCIX")]
        [InlineData(1000, "M")]
        [InlineData(1003, "MIII")]
        [InlineData(1493, "MCDXCIII")]
        [InlineData(1900, "MCM")]
        [InlineData(2000, "MM")]
        [InlineData(3999, "MMMCMXCIX")]
        [InlineData(4000, "Does not exist")]
        [InlineData(9000, "IT'S OVER 9000!!!")]
        public void ConvertCases(int number, string expected)
        {
            string actual = Roman.ConvertToRoman(number);

            Assert.Equal(expected, actual);
        }



    }
}
