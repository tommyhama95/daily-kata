﻿using Xunit;
using Daily_Kata;

namespace Daily_Kata_Test
{
    public class Rectio_Multi_Test
    {
        [Theory]
        [InlineData(0)]
        [InlineData(9)]
        public void ReductoWithSingleDigit_ShouldReturnSameDigit(int expected)
        {
            int actual = Reducto_Multiplicitum.SumDigProd(expected);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ReductoWithTwoNumbers_9And8_ShouldReturn_7()
        {
            int expected = 7;
            int actual = Reducto_Multiplicitum.SumDigProd(9, 8);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ReductoWithTwoDoubleDigitNumbers_16And28_ShouldReturn_6()
        {
            int expected = 6;
            int actual = Reducto_Multiplicitum.SumDigProd(16, 28);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ReductoWithOneHugeNumber_111111111_ShouldReturn_1()
        {
            int expected = 1;
            int actual = Reducto_Multiplicitum.SumDigProd(111111111);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ReductoWithMultiSingleDigits_1Through6_ShouldReturn_2()
        {
            int expected = 2;
            int actual = Reducto_Multiplicitum.SumDigProd(1, 2, 3, 4, 5, 6);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ReductoWithHighNumbers_DoubleAndTriple_ShouldReturn_6()
        {
            int expected = 6;
            int actual = Reducto_Multiplicitum.SumDigProd(26, 497, 62, 841);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(new int[] { 123, -99 }, 8)]
        [InlineData(new int[] { 167, 167, 167, 167, 167, 3 }, 8)]
        [InlineData(new int[] { 98526, 54, 863, 156489, 45, 6156 }, 2)]
        public void ReductoAllInlineData_MixedNumbers_ShouldReturnCorrectValue(int[] numbers, int expected)
        {
            int actual = Reducto_Multiplicitum.SumDigProd(numbers);

            Assert.Equal(expected, actual);
        }

    }

}
