﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Daily_Kata;

namespace Daily_Kata_Test
{
    public class ValidName_Test
    {

        [Theory]
        [InlineData("H. Wells")]
        [InlineData("H. G. Wells")]
        [InlineData("Herbert G. Wells")]
        [InlineData("Herbert George Wells")]

        public void CheckNameValidation_AllCases_ShouldBeValid(string name)
        {
            bool actual = Validation.ValidName(name);

            Assert.True(actual);
        }

        [Theory] 
        [InlineData("Herbert")]
        [InlineData("h. Wells")]
        [InlineData("H Wells")]
        [InlineData("H. George W.")]
        [InlineData("Herb. George Wells")]
        public void CheckNameValidation_AllCases_ShouldbeInvalid(string name)
        {
            bool actual = Validation.ValidName(name);

            Assert.False(actual);
        }
    }
}
