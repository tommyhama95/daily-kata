﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Daily_Kata;
using Xunit;

namespace Daily_Kata_Test
{
    public class BookStore_Test
    {
        [Theory]
        [InlineData(0, new int[] { } )]
        [InlineData(8, new int[] { 1 } )]
        [InlineData(8, new int[] { 2 } )]
        [InlineData(8, new int[] { 3 } )]
        [InlineData(8 * 3, new int[] { 1, 1, 1 } )]
        public void ByBooks_NoDiscount_ShouldReturnPrice(double expected, int[] books)
        {
            double actual = BookStore.Buy(books);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(8 * 2 * 0.95, new int[] { 0, 1 })]
        [InlineData(8 * 3 * 0.9, new int[] { 0, 2, 3 })]
        [InlineData(8 * 4 * 0.8, new int[] { 0, 1, 2, 3 })]
        public void BuyBooks_WithDiscount_ShouldReturnPriceWithDiscount(double expected, int[] books)
        {
            double actual = BookStore.Buy(books);

            Assert.Equal(expected, actual);
        }
    }
}
